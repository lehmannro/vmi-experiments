#include <stdlib.h>
#include <stdio.h>
#include <sys/syscall.h>
#include <signal.h>

unsigned long long i = 0;

void sig_handler(int sig)
{
    printf("\n%llu\n", i);
    exit(0);
}

int main(int argc, const char *argv[])
{
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        return 1;
    for (;;) {
        syscall(SYS_sysfs, 3);
        i++;
    }
    return 0;
}
