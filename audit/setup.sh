#!/bin/bash
CONF=/etc/audisp/plugins.d/script.conf
echo -e "active = yes\ndirection = out\npath = $PWD/script" > $CONF
auditctl -D > /dev/null
auditctl -a entry,always -F arch=b64 -S sysfs
rm -f /tmp/auditlog
/etc/init.d/auditd reload
make test
